﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WebApiCodegen
{
    class Program
    {
        static void Main(string[] args) => CodeGenService.GenerateController(args[0]);
    }
}
