using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WebApiCodegen
{
    public class CodeGenService
    {
        private static string namespaceValue;
        private static List<string> ModelNames = new List<string>();
        static List<string> GetFileNames(string modelFilePath)
        {
            return Directory.GetFiles(modelFilePath).ToList();
        }

        static string GetModelName(string filePath)
        {
            var fileText = File.ReadAllText(filePath);
            return filePath.Replace(".cs", "").Split("\\").Last();
        }

        public static void GenerateController(string modelsFilePath)
        {
            List<string> models = new List<string>();

            var files = GetFileNames(modelsFilePath);
            var templateText = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "\\api_template.txt");
            var namespaceVal = GetNamespace(files.FirstOrDefault());
            foreach (var file in files)
                models.Add(GetModelName(file));

            foreach (var model in models)
            {
                GenerateCsFile(model, namespaceVal, templateText);
            }

        }

        private static void GenerateCsFile(string model, string namespaceVal, string templateText)
        {
            var generatedCode = templateText
            .Replace(Settings.NAMESPACE, namespaceVal)
            .Replace(Settings.MODEL, model)
            .Replace(Settings.MODEL_VAR, Char.ToLowerInvariant(model[0]) + model.Substring(1));

            File.WriteAllText(Directory.GetCurrentDirectory() + $"\\{model}sController.cs", generatedCode);
        }

        private static string GetNamespace(string filePath)
        {
            var fileText = File.ReadAllText(filePath);
            int pFrom, pTo;

            pFrom = fileText.IndexOf("namespace ") + "namespace ".Length;
            pTo = fileText.LastIndexOf(".");
            return fileText.Substring(pFrom, pTo - pFrom);

        }
    }
}